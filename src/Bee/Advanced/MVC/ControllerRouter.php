<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 2/10/2018
 * Time: 12:18 AM
 */

namespace Bee\Advanced\MVC;


use Bee\Core\Requests\Route;
use Bee\Defaults\Routes\HttpServerRouter;

class ControllerRouter extends HttpServerRouter
{
    public function addController(Controller $controller)
    {
        foreach ($controller->getRoutes() as $route) {
            if (empty($route['method'])) {
                /** @noinspection PhpUnhandledExceptionInspection */
                $this->addRoute(Route::create($route['path'], $route['handler']));
            } else {
                /** @noinspection PhpUnhandledExceptionInspection */
                $this->addRoute(Route::create($route['path'], $route['handler'], [$route['method']]));
            }
        }
    }
}