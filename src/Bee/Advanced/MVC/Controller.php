<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 2/9/2018
 * Time: 6:57 PM
 */

namespace Bee\Advanced\MVC;

interface Controller
{
    function getBaseUrl(): string;

    /** @return array { 'method' => 'get', path => '/hello', handler => callable } */
    function getRoutes(): array;
}