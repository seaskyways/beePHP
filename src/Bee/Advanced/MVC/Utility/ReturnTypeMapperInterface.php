<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 2/10/2018
 * Time: 9:55 PM
 */

namespace Bee\Advanced\MVC\Utility;


use Psr\Http\Message\ResponseInterface;

interface ReturnTypeMapperInterface
{
    static function map($response): ResponseInterface;
}