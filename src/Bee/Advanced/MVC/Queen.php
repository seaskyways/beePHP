<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 2/9/2018
 * Time: 6:57 PM
 */

namespace Bee\Advanced\MVC;

use Bee\Advanced\MVC\Utility\StringReturnTypeMapper;
use Bee\Core\Configurable;
use Bee\Core\Configuration;
use Psr\Http\Message\ServerRequestInterface;

class Queen implements Controller, Configurable
{
    protected $returnTypeMappers = [
        "string" => StringReturnTypeMapper::class
    ];

    const CONFIG_RETURN_TYPE_MAPPERS = 'returnTypeMappers';

    function getBaseUrl(): string
    {
        return "/";
    }

    function getRoutes(): array
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $reflect = new \ReflectionClass($this);

        $methods = array_filter(
            $reflect->getMethods(\ReflectionMethod::IS_PUBLIC),
            function (\ReflectionMethod $method) {
                return !in_array($method->getName(), ['getBaseUrl', 'getRoutes']);
            }
        );

        $routes = array_map([$this, 'transformMethodToRoute'], $methods);

        return $routes;
    }

    protected function transformMethodToRoute(\ReflectionMethod $method)
    {
        $split = explode("_", $method->getName(), 2);
        $routeMethod = null;
        if (count($split) === 2) {
            $routeMethod = $split[0];
            $routePath = $split[1];
        } else {
            $routePath = $split[0];
        }
        $closure = $method->getClosure($this);
        $handler = $closure;

        if ($method->hasReturnType() &&
            !empty($returnTypeMapper = $this->returnTypeMappers[$method->getReturnType()->getName()])
        ) {
            $returnTypeMapper = [$returnTypeMapper, 'map'];

            $handler = function (ServerRequestInterface $request, $args) use (&$returnTypeMapper, &$closure) {
                //evaluate route 1
                $result = call_user_func($closure, $request, $args);
                // change type to Response
                return call_user_func($returnTypeMapper, $result);
            };
        }

        return [
            'path' => $this->getBaseUrl() . $routePath,
            'method' => $routeMethod,
            'handler' => $handler,
        ];
    }

    function configure(Configuration $configuration): void
    {
        if ($configuration->has(self::CONFIG_RETURN_TYPE_MAPPERS) &&
            !empty($mappers = $configuration->get(self::CONFIG_RETURN_TYPE_MAPPERS))
        ) {
            foreach ($mappers as $type => $mapper) {
                $this->returnTypeMappers[$type] = $mapper;
            }
        }
    }
}