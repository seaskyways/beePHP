<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/25/2018
 * Time: 9:48 PM
 */

namespace Bee\Defaults\Routes;

use Bee\Core\Configurable;
use Bee\Core\Configuration;
use Bee\Core\Requests\PatternMatcherInterface;
use Bee\Core\Requests\Route;
use Bee\Core\Requests\RouterInterface;
use Bee\Exception\UnrecognizedRoutePattern;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Response;
use React\Promise\Promise;

class HttpServerRouter implements RouterInterface, Configurable
{
    const PATTERNS = "#patterns";
    const VAR_NAME = "#varName";

    /** @var array */
    protected $routeMap = [/*
        "#GET" => null, //callable
        "#patterns" => [
            5 => [ // patterns priorities are used as keys for performance
                "#GET" => null, //callable
                "evenMoreNesting" => [

                ]
            ]
        ],
        "hello" => [
            "#GET" => "func"
        ]
    */];

    /** @var PatternMatcherInterface[] */
    private $patternMatchers = [];

    private $notFoundHandler = null;

    function configure(Configuration $configuration): void
    {
        foreach ($configuration->get(Configuration::ROUTER_PATTERN_MATCHERS) as $patternMatcher) {
            if ($patternMatcher instanceof PatternMatcherInterface) {
                $this->addPatternMatcher($patternMatcher);
            }
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @param array $arguments
     * @return Promise|ResponseInterface
     */
    function onRequest(ServerRequestInterface $request, array $arguments = [])
    {
        $path = $request->getUri()->getPath();
        $routeArgs = [];
        $routeHandler = $this->fetchRouteHandler($path, $request->getMethod(), $routeArgs);

        if (empty($routeHandler)) {
            return call_user_func($this->notFoundHandler, $request);
        } else {
            return call_user_func($routeHandler, $request, $routeArgs);
        }
    }

    protected function fetchRouteHandler($url, $method, &$dynamicRouteArgs = null)
    {
        $subPatterns = $this->splitPath($url);
        $method = strtoupper($method);
        $currentLevelRoutes = &$this->routeMap;

        for ($i = 0; $i < count($subPatterns); $i++) {
            $currentSubPattern = $subPatterns[$i];
            if (empty($currentSubPattern)) continue;

            if (array_key_exists($currentSubPattern, $currentLevelRoutes)) {
                $currentLevelRoutes = &$currentLevelRoutes[$currentSubPattern];
            } else {
                // well the static route isn't found, perhaps it's dynamic
                // check the currentLevel has any patterns
                // check if any pattern matches the input and go with that !
                if (!array_key_exists(self::PATTERNS, $currentLevelRoutes)) {
                    // hit blunt so fast :(
                    return null;
                }
                $patternTags = &$currentLevelRoutes[self::PATTERNS];
                foreach (array_reverse(array_keys($patternTags)) as $priority) {
                    $matcher = $this->patternMatchers[$priority];
                    if ($matcher->match($currentSubPattern)) {
                        $currentLevelRoutes = &$patternTags[$priority];
                        if ($dynamicRouteArgs !== null) {
                            $dynamicRouteArgs[$currentLevelRoutes[self::VAR_NAME]] = $matcher->parse($currentSubPattern);
                        }
                        break;
                    }
                }
            }
        }

        if (array_key_exists("#$method", $currentLevelRoutes)) {
            return $currentLevelRoutes["#$method"];
        }

        return null;
    }

    /**
     * @param Route $route
     * @throws UnrecognizedRoutePattern if the requested pattern is not registered
     */
    function addRoute(Route $route)
    {
        $subPatterns = $this->splitPath($route->getPattern());
        $currentLevelRoutes = &$this->routeMap;
        // open up a path where we want to go
        for ($i = 0; $i < count($subPatterns); $i++) {
            $currentSubPattern = trim($subPatterns[$i], "#\t\n\r\0\x0B");
            // something like "//" would be omitted
            if (empty($currentSubPattern)) continue;

            if ($currentSubPattern[0] === "{") {
                $currentSubPattern = trim($currentSubPattern, "{}");
                list($patternTag, $varName) = explode(":", $currentSubPattern);

                $patternMatcher = $this->getMatcherByTag($patternTag);
                if (empty($patternMatcher)) {
                    throw new UnrecognizedRoutePattern("Unrecognized pattern with tag=$patternTag");
                }

                if (!array_key_exists(self::PATTERNS, $currentLevelRoutes)) {
                    $currentLevelRoutes[self::PATTERNS] = [];
                }
                $currentLevelRoutes = &$currentLevelRoutes[self::PATTERNS];

                if (!array_key_exists($patternMatcher->getPriority(), $currentLevelRoutes)) {
                    $currentLevelRoutes[$patternMatcher->getPriority()] = [
                        self::VAR_NAME => $varName,
                    ];
                }
                $currentLevelRoutes = &$currentLevelRoutes[$patternMatcher->getPriority()];

            } else {
                if (array_key_exists($currentSubPattern, $currentLevelRoutes)) {
                    $currentLevelRoutes = &$currentLevelRoutes[$currentSubPattern];
                } else {
                    $currentLevelRoutes[$currentSubPattern] = [];
                    $currentLevelRoutes = &$currentLevelRoutes[$currentSubPattern];
                }
            }
        }

        foreach ($route->getMethods() as $method) {
            $currentLevelRoutes["#" . strtoupper($method)] = $route;
        }
        echo json_encode($this->routeMap, JSON_PRETTY_PRINT) . PHP_EOL;
    }

    /**
     * @param callable $notFoundHandler
     */
    public function setNotFoundHandler(callable $notFoundHandler): void
    {
        if (!empty($notFoundHandler)) {
            $this->notFoundHandler = $notFoundHandler;
        } else {
            $this->notFoundHandler = function () {
                return new Response(404);
            };
        }
    }

    /**
     * @param string $pattern
     * @return array
     * @throws UnrecognizedRoutePattern
     */
    function compilePattern(string $pattern): array
    {
        $urlSplit = $this->splitPath($pattern);
        $routePatternMatchers = [];
        foreach ($urlSplit as $subPattern) {
            $properMatcher = $this->findMatcher($pattern, $subPattern);
            $routePatternMatchers[] = $properMatcher;
        }
        return $routePatternMatchers;
    }

    /**
     * @param PatternMatcherInterface $patternMatcher
     */
    function addPatternMatcher(PatternMatcherInterface $patternMatcher): void
    {
        $this->patternMatchers[$patternMatcher->getPriority()] = $patternMatcher;
        ksort($this->patternMatchers);
    }

    private function splitPath(string $url)
    {
        return explode("/", $url);
    }

    /**
     * @param string $pattern
     * @param $subPattern
     * @return PatternMatcherInterface|null
     * @throws UnrecognizedRoutePattern
     */
    private function findMatcher(string $pattern, $subPattern)
    {
        $properMatcher = null;
        foreach ($this->patternMatchers as $patternMatcher) {
            if ($patternMatcher->match($subPattern)) {
                $properMatcher = $patternMatcher;
                break;
            }
        }
        if ($properMatcher === null) {
            throw new UnrecognizedRoutePattern("Bad sub-pattern ($subPattern) entered in pattern ($pattern)");
        }
        return $properMatcher;
    }

    protected function getMatcherByTag(string $tag)
    {
        foreach ($this->patternMatchers as $patternMatcher) {
            if ($patternMatcher->getTag() === $tag) return $patternMatcher;
        }
        return null;
    }
}