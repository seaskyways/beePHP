<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/25/2018
 * Time: 11:25 PM
 */

namespace Bee\Defaults\PatternMatchers;


use Bee\Core\Requests\PatternMatcherInterface;

class AnyPatternMatcher implements PatternMatcherInterface
{
    function getPriority(): int
    {
        return 0;
    }

    function getTag(): string
    {
        return "any";
    }

    function match(string $subPattern): bool
    {
        return true;
    }

    function parse(string $subPattern)
    {
        return $subPattern;
    }
}