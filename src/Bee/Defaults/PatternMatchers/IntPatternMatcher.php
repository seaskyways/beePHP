<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/25/2018
 * Time: 11:26 PM
 */

namespace Bee\Defaults\PatternMatchers;


use Bee\Core\Requests\PatternMatcherInterface;

class IntPatternMatcher implements PatternMatcherInterface
{

    function getPriority(): int
    {
        return 10;
    }

    function getTag(): string
    {
        return "int";
    }

    function match(string $subPattern): bool
    {
        return is_numeric($subPattern);
    }

    function parse(string $subPattern)
    {
        return intval($subPattern);
    }
}