<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/30/2018
 * Time: 10:42 PM
 */

namespace Bee\Defaults;


use Bee\Core\Configuration;
use Bee\Core\HttpServer;
use Bee\Core\Requests\RequestHandlerInterface;
use Bee\Core\Requests\RouterInterface;
use Bee\Defaults\PatternMatchers\AnyPatternMatcher;
use Bee\Defaults\PatternMatchers\IntPatternMatcher;
use Bee\Defaults\Routes\HttpServerRouter;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;

class DefaultConfiguration extends Configuration
{

    public function __construct(
        LoopInterface $loop = null,
        HttpServer $server = null,
        RequestHandlerInterface $defaultHandler = null
    )
    {
        $this->addPatternMatcher(new IntPatternMatcher());
        $this->addPatternMatcher(new AnyPatternMatcher());
        $this->set(HttpServer::class, $server ?? new HttpServer());
        $this->set(RequestHandlerInterface::class, $defaultHandler ?? new HttpServerRouter());
        $this->setLoop($loop ?? Factory::create());
        $this->configureConfigurable();
    }

    function getRouter(): RouterInterface
    {
        return $this->get(RequestHandlerInterface::class);
    }

    function getDefaultHandler(): RequestHandlerInterface
    {
        return $this->get(RequestHandlerInterface::class);
    }

    function getServer(): HttpServer
    {
        return $this->get(HttpServer::class);
    }

    function getLoop(): LoopInterface
    {
        return $this->get(LoopInterface::class);
    }
}