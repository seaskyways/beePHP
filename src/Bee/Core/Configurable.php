<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/28/2018
 * Time: 1:55 PM
 */

namespace Bee\Core;


interface Configurable
{
    function configure(Configuration $configuration): void;
}