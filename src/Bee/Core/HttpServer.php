<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/25/2018
 * Time: 8:57 PM
 */

namespace Bee\Core;

use Bee\Core\Requests\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\LoopInterface;
use React\Http\Server as ReactHttpServer;
use React\Socket\Server as ReactSocketServer;


class HttpServer implements Configurable
{
    /** @var ReactHttpServer */
    private $server;
    /** @var RequestHandlerInterface */
    private $defaultRequestHandler;
    /** @var LoopInterface */
    private $loop;

    function configure(Configuration $configuration): void
    {
        $this->loop = $configuration->get(LoopInterface::class);
        $this->defaultRequestHandler = $configuration->get(RequestHandlerInterface::class);
        $requestPreProcessors = $configuration->get(Configuration::REQUEST_PRE_PROCESSORS);
        $this->server = new ReactHttpServer(array_merge($requestPreProcessors, [$this]));
    }

    /**
     * @return ReactHttpServer
     */
    public function getServer(): ReactHttpServer
    {
        return $this->server;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface|\React\Promise\Promise
     */
    private function onRequest(ServerRequestInterface $request)
    {
        return $this->defaultRequestHandler->onRequest($request);
    }

    public function listen(string $uri)
    {
        $socket = new ReactSocketServer($uri, $this->loop);
        $this->server->listen($socket);
    }

    public function __invoke(ServerRequestInterface $request)
    {
        return $this->onRequest($request);
    }
}