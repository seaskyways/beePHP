<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/27/2018
 * Time: 9:38 PM
 */

namespace Bee\Core;


use Bee\Core\Requests\PatternMatcherInterface;
use Bee\Core\Requests\RequestHandlerInterface;
use Psr\Container\ContainerInterface;
use React\EventLoop\LoopInterface;

class Configuration implements ContainerInterface
{
    const REQUEST_PRE_PROCESSORS = 'requestPreProcessors';
    const ROUTER_PATTERN_MATCHERS = 'routerPatternMatchers';

    private $configStorage = [
        LoopInterface::class => null,
        RequestHandlerInterface::class => null,
        HttpServer::class => null,
        self::REQUEST_PRE_PROCESSORS => [],
        self::ROUTER_PATTERN_MATCHERS => [],
    ];


    public function set($key, $value)
    {
        $this->configStorage[$key] = $value;
    }

    public function setLoop(LoopInterface $loop)
    {
        $this->configStorage[LoopInterface::class] = $loop;
    }

    public function setDefaultRequestHandler(RequestHandlerInterface $handler)
    {
        $this->configStorage[RequestHandlerInterface::class] = $handler;
    }

    public function addPreProcessor(callable $preProcessor)
    {
        $this->configStorage[self::REQUEST_PRE_PROCESSORS][] = $preProcessor;
    }

    public function addPatternMatcher(PatternMatcherInterface $patternMatcher)
    {
        $this->configStorage[self::ROUTER_PATTERN_MATCHERS][] = $patternMatcher;
    }

    public function get($key)
    {
        return $this->configStorage[$key];
    }

    public function clear()
    {
        unset($this->configStorage);
    }

    public function configureConfigurable()
    {
        foreach ($this->configStorage as $config) {
            if ($config instanceof Configurable) {
                $config->configure($this);
            }
        }
    }

    public function has($id)
    {
        return array_key_exists($this->configStorage, $id);
    }
}