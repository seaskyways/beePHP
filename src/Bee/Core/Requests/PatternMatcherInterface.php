<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/25/2018
 * Time: 9:41 PM
 */

namespace Bee\Core\Requests;


interface PatternMatcherInterface
{
    function getPriority(): int;

    function getTag(): string;

    function match(string $subPattern): bool;

    function parse(string $subPattern);
}