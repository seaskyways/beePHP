<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/25/2018
 * Time: 9:26 PM
 */

namespace Bee\Core\Requests;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use React\Promise\Promise;

interface RequestHandlerInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param array $arguments
     * @return Promise|ResponseInterface
     */
    function onRequest(ServerRequestInterface $request, array $arguments = []);

}