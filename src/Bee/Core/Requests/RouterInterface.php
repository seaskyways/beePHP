<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/25/2018
 * Time: 9:29 PM
 */

namespace Bee\Core\Requests;

interface RouterInterface extends RequestHandlerInterface
{
    function addRoute(Route $route);

    /**
     * @param string $pattern
     * @return array
     */
    function compilePattern(string $pattern): array;

    /**
     * @param PatternMatcherInterface $patternMatcher
     */
    function addPatternMatcher(PatternMatcherInterface $patternMatcher): void;
}