<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/25/2018
 * Time: 9:33 PM
 */

namespace Bee\Core\Requests;

use Bee\Core\Methods;
use Psr\Http\Message\ServerRequestInterface;


abstract class Route implements RequestHandlerInterface
{
    static public function create(
        string $url,
        callable $handler,
        $methods = [
            Methods::GET,
            Methods::POST,
            Methods::DELETE,
            Methods::PUT,
            Methods::PATCH,
        ]
    ): Route
    {
        return new class($url, $handler, $methods) extends Route
        {
            private $url,
                $handler,
                $methods;

            public function __construct($url, $handler, $methods)
            {
                $this->url = $url;
                $this->handler = $handler;
                $this->methods = $methods;
            }

            function onRequest(ServerRequestInterface $request, array $arguments = [])
            {
                return call_user_func($this->handler, $request, $arguments);
            }

            function getPattern(): string
            {
                return $this->url;
            }

            function getMethods(): array
            {
                return $this->methods;
            }
        };
    }

    abstract function getPattern(): string;

    abstract function getMethods(): array;

    public function __invoke(ServerRequestInterface $request, array $arguments = [])
    {
        return $this->onRequest($request, $arguments);
    }

    public function __toString()
    {
        $pattern = $this->getPattern();
        $methods = implode(",", $this->getMethods());
        return <<< ROUTE
Pattern: $pattern, Methods = [$methods]
ROUTE;
    }
}