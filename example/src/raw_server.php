<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 1/25/2018
 * Time: 9:16 PM
 */

use Bee\Advanced\MVC\ControllerRouter;
use Bee\Core\Requests\Route;
use Psr\Http\Message\ServerRequestInterface;

require_once dirname(dirname(__DIR__)) . "/vendor/autoload.php";

require_once "./IndexQueen.php";
require_once "./InsideQueen.php";

$config = new \Bee\Defaults\DefaultConfiguration(null, null, new ControllerRouter());

/** @var ControllerRouter $router */
$router = $config->getRouter();

$config->setDefaultRequestHandler($router);

$loop = $config->getLoop();

$router->addController(new IndexQueen());
$router->addController(new InsideQueen());

/** @noinspection PhpUnhandledExceptionInspection */
$router->addRoute(Route::create('/hello/{any:0}', function ($req, $args) {
    $d = new \React\Promise\Deferred();
    global $loop;
    $loop->futureTick(function () use (&$d, &$args) {
        $d->resolve(new \React\Http\Response(200, ['Content-Type:application/json'], json_encode($args)));
    });
    return $d->promise();
}));

/** @noinspection PhpUnhandledExceptionInspection */
$router->addRoute(Route::create('/hello/{int:myInt}', function ($req, $args) {
    return new \React\Http\Response(200, [], "Dynamic route int is :" . $args['myInt']);
}, ["GET"]));

$router->setNotFoundHandler(function (ServerRequestInterface $request) {
    return new \React\Http\Response(404, [], 'Not found');
});

$config->getServer()->listen('0.0.0.0:8080');

$config->clear();

$loop->run();