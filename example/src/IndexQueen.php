<?php

use Bee\Advanced\MVC\Queen;
use React\Http\Response;

/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 2/10/2018
 * Time: 12:11 AM
 */
class IndexQueen extends Queen
{
    public function get_index()
    {
        return new Response(200, [], 'Hello queen');
    }
}